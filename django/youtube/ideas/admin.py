from django.contrib import admin
from .models import Idea, Vote

@admin.register(Idea)
class IdeaAdmin(admin.ModelAdmin):
    search_fields = ["title"]
    list_display = ["title", "status"]
    list_filter = ["status"]
    

@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ["id", "idea", "reason"]
    list_filter = ["idea"]
