# Generated by Django 4.0.2 on 2022-03-12 08:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ideas', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='idea',
            name='title',
            field=models.CharField(max_length=255),
        ),
    ]
