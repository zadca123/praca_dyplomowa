# Generated by Django 4.0.2 on 2022-03-10 21:01

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CarSpecs',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('car_body', models.CharField(max_length=50)),
                ('production_year', models.DateField(max_length=50)),
                ('car_model', models.CharField(max_length=50)),
                ('engine_type', models.CharField(max_length=50)),
                ('car_brand', models.CharField(max_length=50)),
            ],
        ),
    ]
