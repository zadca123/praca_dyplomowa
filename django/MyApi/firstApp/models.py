from django.db import models

# Create your models here.

class CarSpecs(models.Model):
    car_body = models.CharField(max_length=50)
    production_year = models.DateField(max_length=50)
    car_model = models.CharField(max_length=50)
    engine_type = models.CharField(max_length=50)
    car_brand = models.CharField(max_length=50)

    def __str__(self):
        return self.car_body
