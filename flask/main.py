from flask import Flask, render_template

app = Flask(__name__)


@app.route("/")
@app.route("/home")
def home_page():
    return render_template("home.html")


@app.route("/image_convert")
def image_convert() -> str:
    return render_template("image_convert.html")


# if __name__ == "__main__":
#     app.debug = True
#     app.run(host="127.0.0.1", port="5000")
